#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    t0lock0 = T0ProcDatasetLock(dataset='/EGamma0', stage='PromptReco')
    t0lock1 = T0ProcDatasetLock(dataset='/EGamma1', stage='PromptReco')

    handler = HTCHandlerByRunDBS(task='alignment-rereco',
                                 dsetname='/EGamma*/*ZElectron*PromptReco*/RAW-RECO',
                                 locks=[t0lock0, t0lock1],
                                 deps_tasks=['alignment-mediate'])

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)