Automation workflow docs: ECAL Z->ee monitoring
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runmon.py
=========

.. argparse::
   :filename: ../runjobs.py
   :func: get_opts
   :prog: runjobs.py

runcml.py
=========

.. argparse::
   :filename: ../runcml.py
   :func: get_opts
   :prog: runcml.py
          
