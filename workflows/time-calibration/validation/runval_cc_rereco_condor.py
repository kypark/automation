#!/usr/bin/env python3
# collect all the files from the fill and create a configuration file
import sys
from TICMeanTimeHandlerCondor import TICMeanTimeHandlerCondor

if __name__ == '__main__':
    handler = TICMeanTimeHandlerCondor(task='timing-cc-val-rereco',
                                 deps_tasks=['timing-cc-rereco'],
                                 prev_input='timing-cc-rereco',
                                 is_cc=True,
                                 is_rereco=True)

    ret = handler()

    sys.exit(ret)
