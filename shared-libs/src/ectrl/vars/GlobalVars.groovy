#!/usr/bin/env groovy
package ectrl.vars

class GlobalVars{
    static def setenv(script, jobname)
    {
        switch(jobname) {
            case ~/.*-prod$/:
                script.env.rctrl_project = "rctrl-prod"
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-ecal-dpg/ecalelfs/automation:prod"
                script.env.image_setup = "source /home/ecalgit/setup.sh; module load lxbatch/tzero"
                script.env.dbinstance = "ecal_prompt_v2"
                script.env.campaign = "prompt,hlt"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/kbdustfxdjyg3b88d96k3xwyoe"
                script.env.ett_notify_url = "https://mattermost.web.cern.ch/hooks/zp7kosu3efnhfkdkekp4nbgr6e"
                script.env.eospath = "/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/\$CAMPAIGN"
                script.env.eosplots = "/eos/home-e/ecalgit/www/prompt-calibration"
                script.env.plotsurl = "https://ecal-automation-plots.web.cern.ch/"
                script.env.t0option = "--t0"
                break
            case ~/.*-repro$/:
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-ecal-dpg/ecalelfs/automation:prod"
                script.env.image_setup = "source /home/ecalgit/setup.sh"
                script.env.dbinstance = "ecal_repro_v1"
                script.env.campaign = "all"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/kbdustfxdjyg3b88d96k3xwyoe"
                script.env.eospath = "/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_repro/\$CAMPAIGN"
                script.env.eosplots = "/eos/home-e/ecalgit/www/prompt-calibration/reprocessing/\$CAMPAIGN"
                script.env.plotsurl = "https://ecal-automation-plots.web.cern.ch/reprocessing/\$CAMPAIGN"
                script.env.t0option = ""
                break
            default:
                script.env.rctrl_project = "rctrl"
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-ecal-dpg/ecalelfs/automation:dev"
                script.env.image_setup = "source /home/ecalgit/setup.sh"       
                script.env.dbinstance = "ecal_rerecos_v1"
                script.env.campaign = "all"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/y95qhxb5a7r49b5m818hjgqssh"
                script.env.ett_notify_url = "https://mattermost.web.cern.ch/hooks/zp7kosu3efnhfkdkekp4nbgr6e"
                script.env.eospath = "/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_replays/\$CAMPAIGN"
                script.env.eosplots = "/eos/home-e/ecalgit/www/prompt-calibration-dev/\$CAMPAIGN"
                script.env.plotsurl = "https://ecal-automation-plots-dev.web.cern.ch/\$CAMPAIGN"
                script.env.t0option = ""
                break
        }
    }
}
